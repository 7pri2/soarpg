class Spell
	attr_reader :range

	# Targets
	NO_ONE 			= 0
	OPPONENT 		= 1
	EVERY_OPPONENTS = 2
	ONE_RANDOM_OP 	= 3
	TWO_RANDOM_OP 	= 4
	THREE_RANDOM_OP = 5
	FOUR_RANDOM_OP 	= 6
	ONE_ALLY 		= 7
	EVERY_ALLY		= 8
	ONE_DEAD_ALLY	= 9
	EVERY_DEAD_ALLY	= 10
	USER			= 11

	def initialize
		@data_skill = nil
		@cooldown = 0
		@current_cooldown = 0
		@range = 0
	end

	def load_from_id(spell_id)
		@data_skill = $data_skills[spell_id]
		load_range
		load_cooldown
	end

	def load_from_RPG_skill(data_skill)
		@data_skill = data_skill
		load_range
		load_cooldown
	end

	def self.within_range(x1, y1, x2, y2, range)
		distance = (x2-x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)
		distance-0.99 <= (range)*(range)
	end

	def cast(casting_bar, target, user, units)
		# We start to check if we have the mana for it
		if @data_skill.mp_cost > user.battler.mp
			Sound.play_buzzer
			return 
		end
		# We build allies and enemies lists
		allies, enemies = Array.new, Array.new
		for unit in units
			if unit.type == Target::ALLY
				allies << unit.character
			elsif unit.type == Target::ENEMY
				enemies << unit.character
			end
		end
		# Check range
		if Spell.within_range(user.pos_x, user.pos_y, target.character.pos_x, target.character.pos_y, @range) # Within range
			# We check if the target is legal
			# TARGET CORRECTION
			case target.type
			when Target::ENEMY # When spell's user targets an enemy
				case legal_targets
				when OPPONENT # If self spell normally targets an enemy
					casting_bar.cast(self, user, [target.character])
				when USER # If self spell normally targets the user
					casting_bar.cast(self, user, [user])
				when ONE_ALLY
					casting_bar.cast(self, user, [user])
				when EVERY_ALLY # If self spell normally targets every allies
					casting_bar.cast(self, user, allies)
				when EVERY_OPPONENTS
					casting_bar.cast(self, user, enemies)
				else # Everything else is impossible at the moment
					Sound.play_buzzer
				end
			when Target::ALLY # When spell's user targets an ally
				case legal_targets
				when USER # If self spell normally targets the user
					casting_bar.cast(self, user, [user])
				when EVERY_ALLY # If self spell normally targets every allies
					casting_bar.cast(self, user, allies)
				when ONE_ALLY
					casting_bar.cast(self, user, [target.character])
				when EVERY_OPPONENTS
					casting_bar.cast(self, user, enemies)
				else # Everything else is impossible at the moment
					Sound.play_buzzer
				end
			end
		else
			case legal_targets
			when USER # If self spell normally targets the user
				casting_bar.cast(self, user, [user])
			when ONE_ALLY
				casting_bar.cast(self, user, [user])
			when EVERY_ALLY # If self spell normally targets every allies
				casting_bar.cast(self, user, allies)
			else
				Sound.play_buzzer
			end
		end
	end

	def resolve(user, targets)
		start_cooldown
		user.resolve_used_spell(@data_skill)
		for t in targets
			t.receive_spell(user, @data_skill)
			if t.battler.dead?
				user.gain_exp(t.exp_on_death)
				drops = t.kill_and_drop
				user.loot(drops)
			end
			t.play_animation(animation_id)
		end
	end

	def is_up?
		@current_cooldown == @cooldown
	end

	def start_cooldown
		@current_cooldown = 0
	end

	def on_cooldown?
		!is_up?
	end

	def remaining_cooldown_seconds
		(@cooldown - @current_cooldown) / 60.0
	end

	def percentage_cooldown
		@current_cooldown.to_f/@cooldown.to_f < 1 ? @current_cooldown.to_f/@cooldown.to_f : 1
	end

	def update_cooldown
		if self.on_cooldown?
			@current_cooldown += 1
			return true
		end
		false
	end

	def load_range
		@range = value_of(@data_skill.note, "range")
	end

	def load_cooldown
		@cooldown = @current_cooldown = value_of(@data_skill.note, "cooldown")
	end

	def name
		@data_skill.name
	end

	def icon
		@data_skill.icon_index
	end

	def range
		@range
	end

	def legal_targets
		@data_skill.scope
	end

	def cooldown
		@cooldown
	end

	def current_cooldown
		@current_cooldown
	end

	def animation_id
		@data_skill.animation_id
	end

	def casting_time
		@data_skill.speed
	end

	def inspect
		"#<Spell @data_skill=" + @data_skill.inspect + ">"
	end

	def value_of(text,field)
		fields = text.split()
		for f in fields
			if f.include?(field)
				return f.split('=')[1].to_i
			end
		end
		0
	end
end