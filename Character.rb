class Character
	attr_reader	:spells
	attr_reader :casting_bar
	attr_reader :casting_bar_view
	attr_reader :event_id
	attr_reader :pos_x
	attr_reader :pos_y
	attr_reader :battler

	PLAYER 	= 0
	IA 		= 1

	ENEMY 	= 0
	ACTOR 	= 1

	SWITCH_COOLDOWN	= 10
	MANA_REGEN_FRAMES_CD = 15

	def initialize(event_id = -1)
		if event_id == -1
			@target_view = TargetView.new
		end

		@battler = nil

		@event_id = event_id
		@type = IA
		@battlertype = nil

		@casting_bar = CastingBar.new
		@casting_bar_view = CastingBarView.new(event_id)
		@status_bar = StatusBar.new(event_id)
		@history_view = HistoryView.new(event_id)
		@casting_bar.set_view(@casting_bar_view)
		@last_spell_tried = nil

		@stats_view = nil
		@dead = false

		@target = nil
		@actor = nil

		@pos_x = 0
		@pos_y = 0

		@frames_until_next_switch = SWITCH_COOLDOWN
		@frames_elapsed = 0	# For mana regeneration

		@spellbar = SpellBar.new(@spells, self)
	end

	def set_spellBar_view(sb_view)
		@spellbar.set_view(sb_view)
	end

	def set_target(new_target)
		@target = new_target
	end

	def load_enemy(enemy_id)
		enemy = $data_enemies[enemy_id]
		@battler_type = ENEMY
		@battler = Game_Enemy.new(0, enemy_id)
		
		@spells = Array.new
		for skill in enemy.actions
			s = Spell.new
			s.load_from_id(skill.skill_id)
			@spells << s
		end

		@spellbar.set_spells(@spells)
	end

	def load_actor(actor_id, type = IA)
		@target = nil
		@battler_type = ACTOR
		@battler = $game_actors[actor_id]
		@type = type

		@spells = Array.new
		for skill in @battler.skills
			s = Spell.new
			s.load_from_RPG_skill(skill)
			@spells << s
		end

		@spellbar.set_spells(@spells)
	end

	def set_spellbar_method(newmethod)
		@spellbar.set_method(newmethod)
	end

	def set_stats_view(view)
		@stats_view = view
	end

	def kill
		$game_map.events[event_id].erase
		@status_bar.update(nil)
		@history_view.clean_all
		@target = nil
		@dead = true
		gain_gold
		$soarpg.remove(self)
	end

	def kill_and_drop
		kill
		drop
	end

	def input_update
		if Input.press?(:_tab) && @frames_until_next_switch == 0
			i = (@target != nil ? $soarpg.units.find_index(@target) + 1 : 0) % $soarpg.units.size
			self.set_target($soarpg.units[i])
			@frames_until_next_switch = SWITCH_COOLDOWN
		else
			@frames_until_next_switch = @frames_until_next_switch - 1 < 0 ? 0 : @frames_until_next_switch - 1
		end
	end

	def update_position
		if event_id != -1
			@pos_x = $game_map.events[event_id].real_x
			@pos_y = $game_map.events[event_id].real_y
		else
			@pos_x = $game_player.real_x
			@pos_y = $game_player.real_y
		end
	end

	def life
		@battler.hp
	end

	def gain_exp(exp)
		if @battler_type == ACTOR
			@battler.gain_exp(exp)
			@history_view.push_exp(exp)
		end
	end

	def exp_on_death
		@battler_type == ENEMY ? @battler.exp : 0
	end

	def loot(drops)
		if @battler_type == ACTOR
			for drop in drops
				@history_view.push_loot(drop)
			end
		end
	end

	def drop
		if @battler_type == ENEMY
			items = @battler.make_drop_items
			for item in items
				$game_party.gain_item(item, 1)
			end
			return items
		end
		nil
	end

	def gain_gold
		if @battler_type == ENEMY
			$game_party.gain_gold(@battler.gold)
		end
	end

	def max_life
		@battler.mhp
	end

	def life_percentage
		life.to_f / max_life.to_f
	end

	def mp
		@battler.mp
	end

	def max_mp
		@battler.mmp
	end

	def mp_percentage
		if max_mp != 0
			return mp.to_f / max_mp.to_f
		else
			return -1
		end
	end

	def update_target_view_if_defined
		if @target_view != nil
			if @last_spell_tried != nil
				@target_view.update(@target, @last_spell_tried.range)
			else
				@target_view.update(@target, 0)
			end
		end
	end

	def receive_spell(user, spell)
		@battler.item_apply(user.battler, spell)
		@history_view.push_damage(@battler.result.hp_damage)
	end

	def resolve_used_spell(spell)
		@battler.use_item(spell)
	end

	def max(a, b)
		a > b ? a : b
	end

	def update
		update_position
		if @type == PLAYER
			input_update
		end
		@status_bar.update(self)
		@casting_bar.update
		@spellbar.update
		@history_view.update
		if(@target != nil && @target.character.is_dead?)
			@target = nil
		end
	end

	def is_dead?
		@dead
	end

	def break_spell
		@casting_bar.break
	end

	def cast_spell(spell_id)
		s = Spell.new
		s.load_from_id(spell_id)
		@last_spell_tried = s
		if @target != nil
			s.cast(casting_bar, @target, self, $soarpg.units)
		end
	end

	def cast_from_bar(spell_id)
		@spellbar.cast(spell_id)
		@last_spell_tried = @spellbar.spells[spell_id]
	end

	def cast_from_grimoire(spell_id)
		if @target != nil
			@spells[spell_id].cast(casting_bar, @target, self, $soarpg.units)
			@last_spell_tried = @spells[spell_id]
		end
	end

	def cast_from_spell(spell)
		if @target != nil
			spell.cast(casting_bar, @target, self, $soarpg.units)
			@last_spell_tried = spell
		end
	end

	def play_animation(anim_id)
		if @event_id < 0
			$game_player.animation_id = anim_id
		elsif @event_id > 0
			$game_map.events[@event_id].animation_id = anim_id
		end
	end

	def inspect
		puts "#<Character"
		puts "@target=" + @target.to_s + ","
		puts "@battler=" + @battler.inspect + ","
		puts "@spells=["
		for spell in @spells
			spell.inspect
		end
		puts "]"
		puts ">,"
		@casting_bar.inspect
		@casting_bar_view.inspect
	end
end