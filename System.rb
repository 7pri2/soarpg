class Soarpg
	attr_reader :player
	attr_reader :units

	def initialize
		@player = Character.new
		@player.load_actor(1, Character::PLAYER)
		@player.set_spellBar_view(SpellBarView.new)
		@player.set_spellbar_method(SpellBar::KEYBOARD)
		@units = Array.new
		@units << Target.new(Target::ALLY, @player)
	end

	def inspect
		for u in @units
			u.inspect
		end
	end

	def remove(unit)
		for u in @units
			if u.character == unit
				@units.delete(u)
			end
		end
	end

	def parse_scene(scene)
		for event in scene.events
			if event[1].list != nil
				for command in event[1].list # Parse instructions in event page
					if command.code == 108 # There is a comment
						comment_words = command.parameters[0].split
						if comment_words[0] == "SOARPG" # This is our comment o/
							u = Character.new(event[1].id)
							case comment_words[1]
							when "Enemy"
								t = Target::ENEMY
							when "Ally"
								t = Target::ALLY
							else
								break
							end
							case comment_words[2]
							when "A"
								u.load_actor(comment_words[3].to_i)
							when "M"
								u.load_enemy(comment_words[3].to_i)
							end
							@units << Target.new(t, u)
						end
					end
				end
			end
		end
	end

	def update
		for unit in @units
			unit.character.update
		end
		# Then draw the targetviews on top of the bars
		for unit in @units
			unit.character.update_target_view_if_defined
		end
	end
end

class Game_Map
	alias soarpg_setup setup
	alias soarpg_update update

	def setup(map_id)
		soarpg_setup(map_id)
		$soarpg = Soarpg.new
		$soarpg.parse_scene(self)
	end

	def update(main = false)
		soarpg_update(main)
		$soarpg.update
		@screen.update
	end
end

class Game_Player
	alias soarpg_move_straight move_straight

	def move_straight(d, turn_ok = true)
		soarpg_move_straight(d, turn_ok)
		$soarpg.player.break_spell
	end
end