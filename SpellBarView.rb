class SpellBarView < Window_Base
	def initialize
		super(0, 0, 200, 50)
		refresh
		contents.font.size = 12
	end

	def update(spells)
		contents.clear
		i = 0
		for spell in spells
			draw_icon(spell.icon, i, 0, 255*spell.percentage_cooldown, spell.remaining_cooldown_seconds)
			if(!spell.is_up?)
				contents.font.color = Color::new(255, 0, 0)
			end
			draw_text(i, 12, 24, 24, index_to_key(spells.index(spell)))
			contents.font.color = Color::new(255, 255, 255)
			#draw_shade(i, 0, 24, 24, 1.0-spell.percentage_cooldown)
			i+=26
		end
		refresh
	end

	def draw_shade(x, y, width, height, percentage)
		@sprite = Sprite.new
		@sprite.bitmap = Bitmap.new(width, height)
		@sprite.bitmap.fill_rect(0, 0, width, height*percentage, Color.new(0,0,0,70))
		@sprite.x = x
		@sprite.y = y
	end

	def draw_icon(icon_index, x, y, opacity, remaining_seconds)
		bitmap = Cache.system("Iconset")
		rect = Rect.new(icon_index % 16 * 24, icon_index / 16 * 24, 24, 24)
		contents.blt(x, y, bitmap, rect, opacity)
		if remaining_seconds != 0
			draw_text(x, y, 24, 24, ('%.1f' % remaining_seconds).to_s)
		end
	end

	def refresh

	end

	def index_to_key(i)
		case i
		when 0
			"q"
		when 1
			"w"
		when 2
			"e"
		when 3
			"r"
		when 4
			"t"
		when 5
			"y"
		when 6
			"u"
		when 7
			"i"
		end
	end
end