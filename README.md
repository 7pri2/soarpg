# SOARPG

SOARPG est un script pour RPG Maker VX Ace qui vise à créer simplement un système de combat basé sur des sorts (à la World of Warcraft).
Le but de ce script est de fournir quelque chose de plug'n'play pour les utilisateurs en ne leur demandant pas de créer des sorts depuis l'interface de scripts, mais en adaptant le script aux interfaces de la BDD de base.
