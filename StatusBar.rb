class StatusBar
	attr_accessor	:event_attached_on

	SPACING	=	2

	def initialize(event_attached_on = -1)
		@event_attached_on = event_attached_on
		@width = 42
		@height = 6
		@sprite = Sprite.new
		@sprite.bitmap = Bitmap.new(@width, @height*2+SPACING)
		refresh
	end

	def attach_to_event(event_id)
		@event_attached_on = event_id
	end

	def inspect
		puts "#<CastingBarView>"
	end

	def update(character)
		@sprite.dispose
		@sprite = Sprite.new
		@sprite.bitmap = Bitmap.new(@width, @height*2+SPACING)

		if(character != nil)
			draw_life(character)
			draw_mp(character)
			if @event_attached_on > 0
				coord_x = $game_map.events[@event_attached_on].screen_x
				coord_y = $game_map.events[@event_attached_on].screen_y
			end
			if @event_attached_on < 0
				coord_x = $game_player.screen_x
				coord_y = $game_player.screen_y
			end
			@sprite.x =  coord_x - @width/2
			@sprite.y =  coord_y - 42
		end
		refresh
	end

	def draw_life(character)
		@sprite.bitmap.fill_rect(0, 0, @width, @height, Color.new(0,0,0))
		@sprite.bitmap.fill_rect(1, 1, (character.life_percentage*(@width-2)), @height-2, Color.new(55, 190, 50))
	end

	def draw_mp(character)
		if character.mp_percentage != -1
			@sprite.bitmap.fill_rect(0, @height+SPACING, @width, @height, Color.new(0,0,0))
			@sprite.bitmap.fill_rect(1, @height+SPACING+1, (character.mp_percentage*(@width-2)), @height-2, Color.new(70, 210, 255))
		end
	end

	def states(character)
		
	end

	def refresh
		Graphics.transition(0)
	end
end