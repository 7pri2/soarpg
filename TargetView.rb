class TargetView
	ICON = 430
	WIDTH	= 544
	HEIGHT 	= 416

	def initialize
		@width = 24
		@height = 24
		@sprite = Sprite.new
		@sprite.bitmap = Bitmap.new(@width, @height)
		refresh
	end

	def update(target, last_spell_range)
		@sprite.bitmap.clear
		if target != nil
			if target.character.event_id == -1
				coord_x = $game_player.screen_x
				coord_y = $game_player.screen_y
				real_x = $game_player.real_x
				real_y = $game_player.real_y
			else
				coord_x = $game_map.events[target.character.event_id].screen_x
				coord_y = $game_map.events[target.character.event_id].screen_y
				real_x = $game_map.events[target.character.event_id].real_x
				real_y = $game_map.events[target.character.event_id].real_y
			end
			#draw_icon(ICON)
			source_x = $game_player.real_x
			source_y = $game_player.real_y
			if Spell.within_range(source_x, source_y, real_x, real_y, last_spell_range)
				draw_line(coord_x, coord_y, Color.new(0, 185, 0))
			else
				draw_line(coord_x, coord_y, Color.new(255, 0, 0))
			end
			#@sprite.x =  coord_x - @width/2
			#@sprite.y =  coord_y - @height*2
		end
		refresh
	end

	def min(a, b)
		a > b ? b : a
	end

	def draw_line(x, y, color)
		pos_x = $game_player.screen_x
		pos_y = $game_player.screen_y
		@sprite.bitmap = Bitmap.new(WIDTH, HEIGHT)
		Line.new(pos_x, pos_y-16, x, y-16, @sprite.bitmap, color)
		@sprite.x = 1
		@sprite.y = 1
=begin
		width = x-pos_x >= 0 ? x-pos_x : pos_x-x
		height = y-pos_y >= 0 ? y-pos_y : pos_y-y
		if(height > 0 && width > 0)
			@sprite.bitmap = Bitmap.new(width, height)
			@sprite.bitmap.fill_rect(0,0, width, height, Color.new(0,0,0))
			@sprite.x = min(x, pos_x)
			@sprite.y = min(y, pos_y)-16
		end
=end
	end

	def draw_icon(icon_index)
		bitmap = Cache.system("Iconset")
		rect = Rect.new(icon_index % 16 * 24, icon_index / 16 * 24, 24, 24)
		@sprite.bitmap.blt(0, 0, bitmap, rect, 255)
		@sprite.tone = Tone.new(255, 0, 0)
	end

	def refresh
		Graphics.transition(0)
	end
end