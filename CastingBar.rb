class CastingBar
	attr_reader	:spell
	attr_reader :last_loaded

	FADE_OUT_TIME	= 500

	def initialize
		@view = nil
		@total_frames = 10
		@current_frames = 0
	end

	def cast_over?
		@current_frames >= @total_frames
	end

	def cast_started?
		@current_frames > 0
	end

	def set_view(barView)
		@view = barView
		notify
	end

	def is_casting?
		@spell != nil
	end

	def cast(spell, user, targets)
		if @spell == nil
			@targets = targets
			@spell = spell
			@total_frames = spell.casting_time
			@current_frames = 0
			@user = user
			notify
			Audio.bgs_play("Audio/BGS/Darkness", 100)
		end
	end

	def percentage_cast
		@current_frames.to_f/@total_frames.to_f < 1 ? @current_frames.to_f/@total_frames.to_f : 1
	end

	def break
		Audio.bgs_fade(FADE_OUT_TIME)
		@spell = nil
		notify
	end

	def update
		if @spell != nil
			@current_frames+=1
			if @current_frames >= @total_frames
				Audio.bgs_fade(FADE_OUT_TIME)
				@spell.resolve(@user, @targets)
				@spell = nil
				@current_frames = 0
			end
			notify
		end
	end

	def notify
		if @view != nil
			@view.update(self)
		end
	end

	def inspect
		puts "#<CastingBar"
		puts "@current_frames:" + @current_frames.to_s + ","
		puts "@total_frames:" + @total_frames.to_s + ","
		print "@spell:"
		if @spell == nil
			puts "nil"
		else
			@spell.inspect
		end
	end
end