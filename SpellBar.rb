class SpellBar
	attr_reader :spells

	KEYBOARD 	= 0
	IA 			= 1

	POLL_COOLDOWN	= 20

	def initialize(spells_list, character, cmethod = IA)
		@spells = spells_list
		@method = cmethod
		@character = character
		@view = nil
		@frames_until_next_poll = POLL_COOLDOWN
	end

	def set_view(view)
		@view = view
		update_view
	end

	def set_method(newmethod)
		@method = newmethod
	end

	def cooldown_reset
		@frames_until_next_poll = POLL_COOLDOWN
	end

	def update_input
		if @frames_until_next_poll == 0
			if Input.press?(:_q)
				cast(0)
			elsif Input.press?(:_w)
				cast(1)
			elsif Input.press?(:_e)
				cast(2)
			elsif Input.press?(:_r)
				cast(3)
			elsif Input.press?(:_t)
				cast(4)
			elsif Input.press?(:_y)
				cast(5)
			elsif Input.press?(:_u)
				cast(6)
			end
		else
			@frames_until_next_poll = @frames_until_next_poll - 1 < 0 ? 0 : @frames_until_next_poll - 1;
		end
	end

	def update
		if @method == KEYBOARD
			update_input
		end
		update_needed = false
		for spell in @spells
			res = spell.update_cooldown
			update_needed = update_needed || res
		end
		if update_needed
			update_view
		end
	end

	def update_view
		if @view != nil
			@view.update(@spells)
		end
	end

	def set_spells(spells)
		@spells = spells
		update_view
	end

	def cast(bar_id)
		cooldown_reset
		if @spells[bar_id] != nil
			if @spells[bar_id].is_up?
				@character.cast_from_spell(@spells[bar_id])
			end
		end
	end
end