class CastingBarView
	attr_accessor :event_attached_on

	def initialize(event_attached_on = -1)
		@event_attached_on = event_attached_on
		@width = 32
		@height = 6
		@sprite = Sprite.new
		@sprite.bitmap = Bitmap.new(@width, @height)
		refresh
	end

	def attach_to_event(event_id)
		@event_attached_on = event_id
	end

	def inspect
		puts "#<CastingBarView>"
	end

	def update(castingbar)
		@sprite.dispose
		@sprite = Sprite.new
		@sprite.bitmap = Bitmap.new(@width, @height)
		if not castingbar.cast_over? and castingbar.cast_started? and castingbar.spell != nil
			@sprite.bitmap.fill_rect(0,0,@width, @height, Color.new(0,0,0))
			@sprite.bitmap.fill_rect(1, 1, (castingbar.percentage_cast*(@width-2)), @height-2, Color.new(255, 80, 80))
			if @event_attached_on > 0
				coord_x = $game_map.events[@event_attached_on].screen_x
				coord_y = $game_map.events[@event_attached_on].screen_y
			end
			if @event_attached_on < 0
				coord_x = $game_player.screen_x
				coord_y = $game_player.screen_y
			end
			@sprite.x =  coord_x - @width/2
			@sprite.y =  coord_y + 2
		end
		refresh
	end

	def refresh
		Graphics.transition(0)
	end
end