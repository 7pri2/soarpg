class Line
	def initialize(x1, y1, x2, y2, bitmap, color)
		if (x1-x2).abs > (y1-y2).abs
			a = x2-x1 != 0 ? (y2-y1)/(x2-x1) : 0
			b = y1-a*x1
			start = x1 < x2 ? x1.to_i : x2.to_i
			stop = x1 < x2 ? x2.to_i : x1.to_i
			for x in [*start..stop]
				y = a*x+b
				bitmap.fill_rect(x, y, 1, 1, color)
			end
		else
			a = y2-y1 != 0 ? (x2-x1)/(y2-y1) : 0
			b = x1-a*y1
			start = y1 < y2 ? y1.to_i : y2.to_i
			stop = y1 < y2 ? y2.to_i : y1.to_i
			for y in [*start..stop]
				x = a*y+b
				bitmap.fill_rect(x, y, 1, 1, color)
			end
		end
	end
end