class Target
	# Target types
	ENEMY	= 0
	ALLY	= 1

	attr_reader	:type
	attr_reader	:character

	def initialize(type, character)
		@type = type
		@character = character
	end

	def inspect
		puts "#<Target"
		puts "@type:" + @type.to_s + ","
		puts "@character:"
		@character.inspect
		puts ">"
	end
end