class HistoryView
	attr_accessor	:event_attached_on

	class Message
		attr_accessor	:remaining_frames

		TOTAL_FRAMES	=	60

		def initialize
			@remaining_frames = TOTAL_FRAMES
		end

		def percentage_elapsed
			(TOTAL_FRAMES.to_f - @remaining_frames.to_f) / TOTAL_FRAMES.to_f
		end

		def tick
			@remaining_frames -= 1
		end

		def draw(sprite, height)
		end

		def draw_icon(icon_index, x, y, content)
			bitmap = Cache.system("Iconset")
			rect = Rect.new(icon_index % 16 * 24, icon_index / 16 * 24, 24, 24)
			content.blt(x, y, bitmap, rect, 255)
		end
	end

	class Damage < Message
		def initialize(amount)
			super()
			@amount = amount
		end

		def draw(sprite, height)
			if(@amount > 0)
				sprite.bitmap.font.color = Color::new(0, 255, 0)
			else
				sprite.bitmap.font.color = Color::new(255, 255, 255)
			end
			sprite.bitmap.font.size = 24
			sprite.bitmap.draw_text(0, percentage_elapsed*height/2, 32, 32, @amount.to_s)
		end
	end

	class Exp < Message
		def initialize(amount)
			super()
			@amount = amount
		end
		
		def draw(sprite, height)
			sprite.bitmap.font.color = Color::new(70, 210, 255)
			sprite.bitmap.font.size = 18
			sprite.bitmap.draw_text(0, height/2-(percentage_elapsed*height/2), 32, 32, "+"+@amount.to_s+"xp")
		end
	end

	class Loot < Message
		def initialize(item)
			super()
			@icon_id = item.icon_index
		end

		def draw(sprite, height)
			draw_icon(@icon_id, 0, height/4-(percentage_elapsed*height/4), sprite.bitmap)
		end
	end

	def initialize(event_attached_on = -1)
		@event_attached_on = event_attached_on
		@width = 32
		@height = 64
		@sprite = Sprite.new
		@sprite.bitmap = Bitmap.new(@width, @height)
		@messages = Array.new
		@update_needed = false
		refresh
	end

	def attach_to_event(event_id)
		@event_attached_on = event_id
	end

	def push_damage(dmg)
		# We invert the sign: negative for damages, positive for healings
		@messages.push(Damage.new(-dmg))
		@update_needed = true
	end

	def push_exp(xp)
		@messages.push(Exp.new(xp))
		@update_needed = true
	end

	def push_loot(item)
		@messages.push(Loot.new(item))
		@update_needed = true
	end

	def update()
		@sprite.dispose
		@sprite = Sprite.new
		@sprite.bitmap = Bitmap.new(@width, @height)
		draw_messages

		if @event_attached_on > 0
			coord_x = $game_map.events[@event_attached_on].screen_x
			coord_y = $game_map.events[@event_attached_on].screen_y
		end
		if @event_attached_on < 0
			coord_x = $game_player.screen_x
			coord_y = $game_player.screen_y
		end
		@sprite.x =  coord_x - @width/2
		@sprite.y =  coord_y - @height/2

		refresh
	end

	def clean_all
		@sprite.dispose
		refresh
	end

	def draw_messages
		for m in @messages
			m.draw(@sprite, @height)
			m.tick
			if m.remaining_frames <= 0
				@messages.delete(m)
			end
		end
	end

	def refresh
		Graphics.transition(0)
	end

	def inspect
		"#{}<HistoryView= @messages" + @messages.inspect + ">"
	end
end